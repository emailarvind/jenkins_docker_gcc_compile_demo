CC      = gcc
CFLAGS  = -g
RM      = rm -f


default: all

all: build test

build: src/add.c
	$(CC) $(CFLAGS) src/add.c -o add

test1: add
	./add 10 20 

test2: add
	./add 100 200
	
test3: add
	./add 1000 2000	

clean:
	$(RM) add